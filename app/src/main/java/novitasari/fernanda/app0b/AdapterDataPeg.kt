package novitasari.fernanda.app0b

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*


class AdapterDataPeg(val dataPeg: List <HashMap<String,String>>,
                     val mainActivity: MainActivity) : //new
    RecyclerView.Adapter<AdapterDataPeg.HolderDataPeg>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderDataPeg {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_pegawai, p0, false)
        return HolderDataPeg(v)
    }

    override fun getItemCount(): Int {
        return dataPeg.size
    }

    override fun onBindViewHolder(p0: AdapterDataPeg.HolderDataPeg, p1: Int) {
        val data = dataPeg.get(p1)
        p0.txKode.setText(data.get("kode"))
        p0.txNama.setText(data.get("nama"))
        p0.txJenKel.setText(data.get("jenkel"))
        p0.txStatus.setText(data.get("status_peg"))


        //beginNew
        if (p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230, 245, 240)
        )
        else p0.cLayout.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.cLayout.setOnClickListener(View.OnClickListener {

            val pos1 = mainActivity.daftarstatus.indexOf(data.get("status_peg"))
            mainActivity.spinStatus.setSelection(pos1)
            mainActivity.edKode.setText(data.get("kode"))
            mainActivity.edNamaPeg.setText(data.get("nama"))
            mainActivity.edJenkel.setText(data.get("status"))
            Picasso.get().load(data.get("url")).into(mainActivity.imUpload)
        })
        //endNew
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    class HolderDataPeg(v: View) : RecyclerView.ViewHolder(v) {
        val txKode = v.findViewById<TextView>(R.id.txKode)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txJenKel = v.findViewById<TextView>(R.id.txJenKel)
        val txStatus = v.findViewById<TextView>(R.id.txStatus)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout) //new
    }
}