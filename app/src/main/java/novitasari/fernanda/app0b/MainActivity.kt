package novitasari.fernanda.app0b

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_pegawai.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelper : MediaHelper
    lateinit var pegAdapter: AdapterDataPeg
    lateinit var statusAdapter : ArrayAdapter<String>
    var daftarPeg = mutableListOf<HashMap<String,String>>()
    var daftarStatus = mutableListOf<HashMap<String,String>>()
    var daftarstatus = mutableListOf<String>()
   // var url = "http://192.168.43./kantor/show_data.php"
   // var url1 = "http://192.168.43.47/kantor/query_upd_del_ins.php"
    //val url2 = "http://193.168.43.47/kantor/upload.php"
   var url = "http://192.168.43.228/kantor/show_data.php"
    var url1 = "http://192.168.43.228/kantor/query_upd_del_ins.php"
    val url2 = "http://193.168.43.228/kantor/upload.php"
    var imstr = ""
    var namafile = ""
    var pilihStatus = ""
    var fileUri = Uri.parse("")

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate ->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete ->{
                queryInsertUpdateDelete("delete")
            }
            R.id.btnFind ->{
                showDataPeg(edNamaPeg.text.toString().trim())
            }
            R.id.btnInsertCamera ->{
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.btnInsertCamera ->{
                requestPermissions()
            }
            R.id.btnSend ->{
                uploadFile()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setContentView(R.layout.activity_pegawai)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        //mediaHelper = MediaHelper()
        //`@+id/btnInsertCamera`.setOnClickListener(this)
         // btnSend.setOnClickListener(this)

        pegAdapter = AdapterDataPeg(daftarStatus,this) //new
        mediaHelper = MediaHelper()
        listPeg.layoutManager = LinearLayoutManager(this)
        listPeg.adapter = pegAdapter

        statusAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarstatus)
        spinStatus.adapter = statusAdapter
        spinStatus.onItemSelectedListener = itemSelected1

        imUpload.setOnClickListener(this)
//       listMhs.addOnItemTouchListener(itemTouch) #new
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataPeg("")
        getStatus()
    }

    val itemSelected1 = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {

            spinStatus.setSelection(0)
            pilihStatus = daftarstatus.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihStatus = daftarstatus.get(position)
        }

    }

    fun uploadFile(){
        val request = object : StringRequest(Method.POST,url,
            Response.Listener { response ->
                val jsonObject = JSONObject(response);
                val kode = jsonObject.getString("kode")
                if(kode.equals("000")){
                    Toast.makeText(this, "Upload foto sukses", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this, "Upload foto gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("imstr",imstr)
                hm.put("namafile", namafile)
                return hm
            }
        }
        val q = Volley.newRequestQueue(this)
        q.add(request)
    }

    fun requestPermissions() = runWithPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
            fileUri = mediaHelper.getOutputMediaFileUri()
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
            startActivityForResult(intent,mediaHelper.getRcCamera())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK)
            if(requestCode == mediaHelper.getRcCamera()){
                imstr = mediaHelper.getBitmapToString(imV,fileUri)
                namafile = mediaHelper.getMyFileName()
            }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url1,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataPeg("")
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("kode",edKode.text.toString())
                        hm.put("nama",edNamaPeg.text.toString())
                        hm.put("jenkel",edJenkel.text.toString())
                        hm.put("status",pilihStatus)
                        hm.put("image",imstr)
                        hm.put("file",nmFile)

                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("kode",edKode.text.toString())
                        hm.put("nama",edNamaPeg.text.toString())
                        hm.put("jenkel",edJenkel.text.toString())
                        hm.put("status",pilihStatus)
                        hm.put("image",imstr)
                        hm.put("file",nmFile)

                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("kode",edKode.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getStatus(){
        val request = StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarstatus.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarstatus.add(jsonObject.getString("jk"))
                }
                statusAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataPeg(nama: String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarPeg.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String,String>()
                    mhs.put("kode",jsonObject.getString("kode"))
                    mhs.put("nama",jsonObject.getString("nama"))
                    mhs.put("url",jsonObject.getString("url"))
                    mhs.put("status",jsonObject.getString("status"))
                    mhs.put("jenkel",jsonObject.getString("jenkel"))
                    daftarPeg.add(mhs)
                }
                pegAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama",nama)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}
